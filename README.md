# Description
This is just a personal project of mine, it contains a server written in C and an esp32 program that sends temperature values to it.

ps: the esp32 is powered by a solar panel, which makes it even more epic.

ps ps: the data from the server is sent to an oled panel

# main.c

this is the server

# esp32_client.ino

this has to be flashed onto the esp32

# oled.py

this is a programm written for the usage of an lc-display on a raspberry pi