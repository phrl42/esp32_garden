#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
int main()
{
  int socketDesc;
  int addrlen;
  int socketPending;
  struct sockaddr_in address;

  int buffersize = 1024;
  char *buffer = malloc(buffersize);

  FILE *fp;

  socketDesc = socket(AF_INET, SOCK_STREAM, 0);
 
  /* error checking */
  if(socketDesc == 0)
  {
    perror("couldn't create socket");
  }

  /* master socket initialization */
  /* give address struct the properties of our socket */
  
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = htonl(INADDR_ANY); /* what does this do? */

  address.sin_port = htons(7000); /* converts short to network byte */

  /* for more information, see man 3 bind ;; bound socket to local address */
  /* give socketDesc || give address family reference || get the struct length */
  bind(socketDesc, (struct sockaddr *)&address, sizeof(address));
  
  while(1)
  {
    /* listen to incoming connections ;; 3 = max connections */
    listen(socketDesc, 3);
  
    /* accept incoming connections && wait if there is no connection */
    addrlen = sizeof(address);
    socketPending = accept(socketDesc, (struct sockaddr *)&address, (socklen_t *restrict)&addrlen);
  
    if(socketPending < 0)
    {
      perror("can't accept incoming connections");
    }

    printf("New socket: %d\n", socketPending);
    /* open the file pointer */ 
    fp = fopen("esp32temp", "w");

  /* receive a message from the incoming connection / client */
    recv(socketPending, buffer, buffersize, 0);
    
    /* write to file */
    fputs(buffer, fp);
    printf("%s\n", buffer);
    
    /* close file */
    fclose(fp);
    close(socketPending);
  }
  /* properly close the binded socket */
  close(socketDesc);
  return 0;
}