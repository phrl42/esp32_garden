import time
import subprocess
import board
import adafruit_ssd1306
from PIL import Image, ImageDraw, ImageFont

# short explanation: this is just a loop that renders an image with the specific text (variables,...)
#                    and therefore it just parses it to the lc-display, which is really cool and useful
# display parameters
WIDTH = 128
HEIGHT = 64
x = 0

# I2C initialization
i2c = board.I2C()

# specify device location (0x3c) and parameters
oled = adafruit_ssd1306.SSD1306_I2C(WIDTH, HEIGHT, i2c, addr=0x3c, reset=0)

# clear display
oled.fill(0)
# show display
oled.show()

# create an image
# 1 stands for 1 bit picture, this is crucial...
img = Image.new('1', (oled.width, oled.height))

# declare draw(ing) context
draw = ImageDraw.Draw(img)

fontsize = 13
font = ImageFont.truetype('mononoki.ttf', fontsize)

# draw white foreground (I think?)
draw.rectangle((0, 0, oled.width, oled.height), outline=255, fill=255)

while True:
    # ----------------------------------typical render loop----------------------------------
    cmd = "free -h | awk '/Mem/ {printf $3}'"    
    ram = subprocess.check_output(cmd, shell = True)

    cmd = "sensors |awk '/temp1/ {print $2 $3}'"
    tmp = subprocess.check_output(cmd, shell = True)
   
    cmd = "stat=$(top -n 1 -bn2 |awk '/%Cpu/ {print $2}' | tail -n 1) && echo $stat%"
    cpu = str(subprocess.check_output(cmd, shell = True), 'utf-8')

    cmd = "cat esp32temp | tail -n 1"
    outsideTemp = str(subprocess.check_output(cmd, shell = True), 'utf-8')
    
    # black box fills the screen (clear)        
    draw.rectangle((0, 0, oled.width, oled.height), outline=0, fill=0)

    # self-explanatory
    draw.text((x, x + (fontsize * 0)), "RAM: " + str(ram, 'utf-8'), font=font, fill=255)
    draw.text((x, x + (fontsize * 1)), str(tmp, 'utf-8'), font=font, fill=255)              
    draw.text((x, x + (fontsize * 2)), cpu, font=font, fill=255)
    draw.text((x, x + (fontsize * 3)), outsideTemp, font=font, fill=255)

    # render the image (rendercopy)
    oled.image(img)
    
    # switch back buffer with front buffer (show changes)
    oled.show()

    # delay the rendering, otherwise it would implode
    time.sleep(.1)