#include <WiFi.h>

//------- led definitions----------------
#define LED  2

//------- some sensor definition---------
#ifdef __cplusplus
extern "C" 
{
  #endif  
  uint8_t temprature_sens_read(); // I am aware of this poor grammar (just unsure if change is good)
  #ifdef __cplusplus
}
#endif
//-------- declarations of variables------
// needed for conversion of microseconds to seconds
#define factor 1000000
#define sleepTime 300

WiFiClient client;
const char* host = "insertserverip";
const uint16_t port = 7000;

const char* ssid = "yourssid";
const char* pass = "glowmore";

uint8_t tempF;
float tempC;
float compare;

int sendIntervall = 1;

char *add = "°C";

void connect()
{
  //-----wifi stuff-----------------
  WiFi.begin(ssid, pass);
  while(WiFi.status() != WL_CONNECTED)
  {
    printf("...");
    digitalWrite(LED, LOW);
  }
  Serial.print("\n");
  Serial.print(WiFi.localIP());
  Serial.print("\n");
}

void sendData()
{
  digitalWrite(LED, LOW);
  
  // check wifi connection
  if(WiFi.status() != WL_CONNECTED)
  {
    connect();
  }

  // check server connection

  //-----server connectivity stuff-----------
  if(!client.connect(host, port))
  {
    Serial.print("could not connect to server\n");
    delay(10);
    return;
  }
  
  tempF = temprature_sens_read();
  tempC = ((tempF - 32) / 1.8) - 40;
  
  // filter
  
  if(tempC > 13.2 && tempC < 13.4)
  {
    return;
  }
    
  Serial.print("Temperature: ");
  Serial.print(String(tempC, 2));
  Serial.print("\n");
  
  digitalWrite(LED, HIGH);
  
  client.print(String(tempC, 2) + add);
  // disconnect from wifi
  client.stop();
  
  // set led to low because h
  digitalWrite(LED, LOW);
}

// after rebooting from deep_sleep the esp32 starts at setup() once again
void setup() 
{
  sendIntervall = 1;
  pinMode(LED, OUTPUT);
  Serial.begin(115200);
  connect();
}

void loop()
{
  sendData();

  if(sendIntervall == 3)
  {
    esp_sleep_enable_timer_wakeup(sleepTime * factor);
    esp_deep_sleep_start();
  }
  sendIntervall++;
}